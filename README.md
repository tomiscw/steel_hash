# Steel Hash

Steel Hash is a raw hash generator written in Rust. It uses Lipsum as the source with the number of words generated ranging from 999 to 99,999 (as of this writing). The library supports Blake2, SHA-2 and SHA-3. It also includes a ``_combo`` variant of these respective algorithms which uses all three as a source to generate a hash.


## Disclaimer

As I'm not a expert in this field, I can't guarantee this method is secure, even if the algorithms supported are, but it should suffice for consumer-grade software.