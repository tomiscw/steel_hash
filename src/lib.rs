// This file is licensed to you under the MPL 2.0 license.
// See the LICENSE file in the project root for more information.
extern crate blake2;
extern crate sha3;
extern crate sha2;
extern crate lipsum;
extern crate rand;

use blake2::Blake2b;
use sha2::Sha256;
use sha3::Sha3_256;
use lipsum::lipsum;
use rand::{Rng, thread_rng};

/// Warning: this generates a large amount of strings
pub fn hashing_source() -> String {
    let min = 999;
    let max = 99999;
    let range = thread_rng().gen_range(min, max);
    format!("{}", lipsum(range))
}

/// Generates a raw Blake2 hash
pub fn generate_blake2() -> String {
    use blake2::Digest;
    let mut hasher = Blake2b::new();
    hasher.input(hashing_source().as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

/// Generates a raw 256 variant of Sha2.
pub fn generate_sha2() -> String {
    use sha2::Digest;
    let mut hasher = Sha256::new();
    hasher.input(hashing_source().as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

/// Generates raw 256 variant of Sha3.
pub fn generate_sha3() -> String {
    use sha2::Digest;
    let mut hasher = Sha3_256::new();
    hasher.input(hashing_source().as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

/// Generates a raw Blake2 hash based on itself, Sha2 and Sha3
pub fn generate_blake2_combo() -> String {
    use blake2::Digest;
    let mut hasher = Blake2b::new();
    let combo = format!("{}{}{}", generate_sha2(), generate_blake2(), generate_sha3());
    hasher.input(combo.as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

/// Generates a raw Sha2 hash based on itself, Blake2 and Sha3
pub fn generate_sha2_combo() -> String {
    use blake2::Digest;
    let mut hasher = Sha256::new();
    let combo = format!("{}{}{}", generate_sha2(), generate_blake2(), generate_sha3());
    hasher.input(combo.as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

/// Generates a raw Sha3 hash based on itself, Blake2 and Sha2
pub fn generate_sha3_combo() -> String {
    use blake2::Digest;
    let mut hasher = Sha3_256::new();
    let combo = format!("{}{}{}", generate_sha2(), generate_blake2(), generate_sha3());
    hasher.input(combo.as_str().as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}