extern crate steel_hash;

use steel_hash::{generate_blake2_combo, generate_sha2_combo, generate_sha3_combo};

#[test]
fn test_blake2_combo() {
    let hash = &generate_blake2_combo();
    let hash2 = hash;
    println!("Generated Blake2 Combo:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}

#[test]
fn test_sha2_combo() {
    let hash = &generate_sha2_combo();
    let hash2 = hash;
    println!("Generated Sha2 Combo:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}

#[test]
fn test_sha3_combo() {
    let hash = &generate_sha3_combo();
    let hash2 = hash;
    println!("Generated Sha3 Combo:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}