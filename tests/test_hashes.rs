extern crate steel_hash;

use steel_hash::{generate_blake2, generate_sha2, generate_sha3};

#[test]
fn test_blake2() {
    let hash = &generate_blake2();
    let hash2 = hash;
    println!("Generated Blake2:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}

#[test]
fn test_sha2() {
    let hash = &generate_sha2();
    let hash2 = hash;
    println!("Generated Sha2:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}

#[test]
fn test_sha3() {
    let hash = &generate_sha3();
    let hash2 = hash;
    println!("Generated Sha3:\n{}", hash);
    assert_eq!(hash, hash2);
    assert!(true);
}
